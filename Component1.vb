Public Class Component1
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents FSW1 As System.IO.FileSystemWatcher
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.FSW1 = New System.IO.FileSystemWatcher()
        CType(Me.FSW1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'FSW1
        '
        Me.FSW1.EnableRaisingEvents = True
        CType(Me.FSW1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Dim p

    Protected Sub OnPause()
        Me.FSW1.EnableRaisingEvents = False
    End Sub

    Protected Sub OnContinue()
        Me.FSW1.EnableRaisingEvents = True
    End Sub

    Private Sub FSW1_Created(ByVal sender As Object, ByVal e As System.IO.FileSystemEventArgs) Handles FSW1.Created
        'MsgBox("File is here", MsgBoxStyle.OKOnly, Me.ServiceName)
        Dim r As Integer
        'Me.EventLog.WriteEntry("File " & e.FullPath & " detected.")
        'p = New parser()
        'r = p.GetFile(e.FullPath)
        'Me.EventLog.WriteEntry("File processing returned " & Str(r) & " code.")
        'p = Nothing
    End Sub

 
End Class
