Imports System.Xml
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib

Public Class AddArticle32
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()
        'Add any initialization after the InitializeComponent() call

        Try
            InitGlobals()
        Catch err As Exception
            LogFile.WriteErr(SimpleService.errFilePath, "Error in AddArticel.InitGlobals", err)
        End Try

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapterAddArticle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDataAdapterAddSubrel As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlInsertCommandNewArticle As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommandAddSubrel As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand1x As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlDataAdapterAddArticle = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommandNewArticle = New System.Data.SqlClient.SqlCommand()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.SqlDataAdapterAddSubrel = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommandAddSubrel = New System.Data.SqlClient.SqlCommand()
        Me.SqlCommand1x = New System.Data.SqlClient.SqlCommand()
        '
        'SqlDataAdapterAddArticle
        '
        Me.SqlDataAdapterAddArticle.InsertCommand = Me.SqlInsertCommandNewArticle
        Me.SqlDataAdapterAddArticle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "newArticle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefID", "RefID")}), New System.Data.Common.DataTableMapping("Table1", "Table1", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefID", "RefID")})})
        '
        'SqlInsertCommandNewArticle
        '
        Me.SqlInsertCommandNewArticle.CommandText = "[addNewArticle]"
        Me.SqlInsertCommandNewArticle.CommandType = System.Data.CommandType.StoredProcedure
        Me.SqlInsertCommandNewArticle.Connection = Me.SqlConnection1
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CreationDateTime", System.Data.SqlDbType.DateTime, 8))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Urgency", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(3, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HasSounds", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(3, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HasPictures", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(3, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Maingroup", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Subgroup", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Categories", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Subcategories", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(19, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HasReferences", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticleTitle", System.Data.SqlDbType.VarChar, 255))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ingress", System.Data.SqlDbType.VarChar, 1024))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticleXML", System.Data.SqlDbType.Text))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Evloc", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CountyDist", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Debug", System.Data.SqlDbType.VarChar, 255))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NTB_ID", System.Data.SqlDbType.VarChar, 32))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KeyWordID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NtbFolderID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, ""))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SoundFileName", System.Data.SqlDbType.VarChar, 255))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KeyWord", System.Data.SqlDbType.VarChar, 16))
        Me.SqlInsertCommandNewArticle.Parameters.Add(New System.Data.SqlClient.SqlParameter("@isHidden", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(3, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "data source=DEVSERVER1;initial catalog=ntb;password=password;persist security inf" & _
        "o=True;user id=sa;workstation id=GX240-PORTAL-0;packet size=4096"
        '
        'SqlDataAdapterAddSubrel
        '
        Me.SqlDataAdapterAddSubrel.InsertCommand = Me.SqlInsertCommandAddSubrel
        Me.SqlDataAdapterAddSubrel.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "newArticle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefID", "RefID")}), New System.Data.Common.DataTableMapping("Table1", "Table1", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefID", "RefID")})})
        '
        'SqlInsertCommandAddSubrel
        '
        Me.SqlInsertCommandAddSubrel.CommandText = "[addSubRel]"
        Me.SqlInsertCommandAddSubrel.CommandType = System.Data.CommandType.StoredProcedure
        Me.SqlInsertCommandAddSubrel.Connection = Me.SqlConnection1
        Me.SqlInsertCommandAddSubrel.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandAddSubrel.Parameters.Add(New System.Data.SqlClient.SqlParameter("@refid", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandAddSubrel.Parameters.Add(New System.Data.SqlClient.SqlParameter("@cat", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommandAddSubrel.Parameters.Add(New System.Data.SqlClient.SqlParameter("@subcat", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlCommand1x
        '
        Me.SqlCommand1x.Connection = Me.SqlConnection1

    End Sub

#End Region

    ' Global variables:
    Private strDebug As String
    Private strDebugFile As String
    Private sqlBitnames As GetBitnames

    Private intSqlImportNtbFolder As Integer
    Private intSqlImportMaingroup As Integer
    Private intSqlImportSubgroup As Integer

    Private intSqlNotHiddenNtbFolder As Integer
    Private intSqlNotHiddenMaingroup As Integer

    Private intAutonomyImportNtbFolder As Integer
    Private intAutonomyImportMaingroup As Integer
    Private intAutonomyImportSubgroup As Integer

    Private intAutonomyArchiveNtbFolder As Integer
    Private intAutonomyArchiveMaingroup As Integer
    Private intAutonomyArchiveSubgroup As Integer

    Private bAutonomyImportEnable As Boolean
    Private bAutonomyArchiveEnable As Boolean

    Private AutonomyIdxImportPath As String
    Private AutonomyIdxArchivePath As String

    Public sqlConnectionString As String
    Private fileIdx As AutonomyIDX

    Private Sub InitGlobals()
        ' Any Initialization code here:
        ' Fill Bit-pattern Hashtables
        LogFile.WriteLog(SimpleService.errFilePath, "Er i InitGlobals (AddArticle32.vb)")
        sqlConnectionString = AppSettings("SqlConnectionString")

        sqlBitnames = New GetBitnames()
        sqlBitnames.Init(sqlConnectionString)

        ' New Code 25.11.2002 RoV:
        fileIdx = New AutonomyIDX(sqlConnectionString)

        ' Get Autonomy IDX File destination path
        AutonomyIdxImportPath = AppSettings("AutonomyIdxImportPath")
        If Not Directory.Exists(AutonomyIdxImportPath) Then
            Directory.CreateDirectory(AutonomyIdxImportPath)
        End If

        ' Get Autonomy IDX File destination path
        AutonomyIdxArchivePath = AppSettings("AutonomyIdxArchivePath")
        If Not Directory.Exists(AutonomyIdxArchivePath) Then
            Directory.CreateDirectory(AutonomyIdxArchivePath)
        End If

        ' Get Import selection bitpatterns
        intSqlImportNtbFolder = AppSettings("SqlImportNtbFolder")
        intSqlImportMaingroup = AppSettings("SqlImportMaingroup")
        intSqlImportSubgroup = AppSettings("SqlImportSubgroup")

        ' Get Files Not to be hidden bitpatterns
        intSqlNotHiddenNtbFolder = AppSettings("SqlNotHiddenNtbFolder")
        intSqlNotHiddenMaingroup = AppSettings("SqlNotHiddenMaingroup")

        ' Boolean variable for choosing to make Autonomy import files or not
        bAutonomyImportEnable = (LCase(AppSettings("AutonomyImportEnable")) = "true") Or (LCase(AppSettings("AutonomyImportEnable")) = "1") Or (LCase(AppSettings("AutonomyImportEnable")) = "yes")
        ' Bit pattern variables for choosing to make Autonomy import files or not
        intAutonomyImportNtbFolder = AppSettings("AutonomyImportNtbFolder")
        intAutonomyImportMaingroup = AppSettings("AutonomyImportMaingroup")
        intAutonomyImportSubgroup = AppSettings("AutonomyImportSubgroup")

        ' Boolean variable for choosing to make Autonomy import files or not
        bAutonomyArchiveEnable = (LCase(AppSettings("AutonomyArchiveEnable")) = "true") Or (LCase(AppSettings("AutonomyArchiveEnable")) = "1") Or (LCase(AppSettings("AutonomyArchiveEnable")) = "yes")
        ' Bit pattern variables for choosing to make Autonomy import files or not
        intAutonomyArchiveNtbFolder = AppSettings("AutonomyArchiveNtbFolder")
        intAutonomyArchiveMaingroup = AppSettings("AutonomyArchiveMaingroup")
        intAutonomyArchiveSubgroup = AppSettings("AutonomyArchiveSubgroup")

        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med � kj�re InitGlobals i addarticle32.vb")
    End Sub

    Public Sub RefreshKeywords()
        LogFile.WriteLog(SimpleService.errFilePath, "StarterRefreshKeywords")
        sqlBitnames.RefreshKeywords()
        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med RefreshKeywords")
    End Sub

    Public Sub Open()
        LogFile.WriteLog(SimpleService.errFilePath, "i Sub Open")
        Me.SqlConnection1.ConnectionString = sqlConnectionString
        Me.SqlConnection1.Open()
        'Me.SqlConnection1.ConnectionTimeout = 150
        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med sub Open")
    End Sub

    Public Sub Close()
        Me.SqlConnection1.Close()
    End Sub

    Public Function AddNewArticle(ByVal strFilename As String, ByRef e As Exception, ByRef CreationDateTime As DateTime) As String
        ' Main function for importing articles from NITF-XML files into SQL Server, 
        ' Also produces Autonomy IDX importfiles

        Dim intCategories As Integer = 0
        Dim intSubcategories As Int64 = 0
        Dim intHasReferences As Integer = 0
        Dim intArticleId As Integer
        Dim arrSubrels(1, 20) As Int64
        '        Dim strAutonomyEtc As String = ""
        Dim i As Integer

        strDebugFile = strFilename
        strDebug = Path.GetFileName(strFilename) & ", "

        ' Read XML-document from file
        Dim strXmlDoc As String
        Dim xmlDoc As XmlDocument = New XmlDocument
        Try
            strXmlDoc = LogFile.ReadFile(strFilename)

            ' Bugfix to handle bug in NITF from Fretex
            strXmlDoc = Replace(strXmlDoc, vbCrLf & "</xtable>", "")
            'xmlDoc.Load(strFilename)
            xmlDoc.LoadXml(strXmlDoc)
        Catch e2 As Exception
            ' On error: return Error object
            e = e2
            Return "error"
        End Try

        ' Exclude ntb-kanal="N" for Nordic line:
        Try
            If xmlDoc.SelectSingleNode("nitf/head/meta[@name='NTBKanal']/@content").InnerText = "N" Then
                ' Skips files to the Nordic canal
                Return "skip"
            End If
        Catch
            ' Continnue
        End Try

        ' Exclude ntb-meldingsType="Meny-melding" for Worklists
        Dim intNtbFolderID As Integer = GetNtbFolder(xmlDoc)
        If intNtbFolderID = 0 Then
            ' If intNtbFolder not found set like "Ut-Satellitt"
            intNtbFolderID = 1
        End If

        Try
            Dim strMeldingsType As String = ""
            Try
                strMeldingsType = xmlDoc.SelectSingleNode("nitf/head/meta[@name='NTBMeldingsType']/@content").InnerText
            Catch
            End Try
            ' Folder = "Ut-Portal" 
            If intNtbFolderID = 1024 And strMeldingsType = "Meny-melding" Then
                ' Files to "Arbeidslister"
                Dim strType As String = xmlDoc.SelectSingleNode("nitf/head/tobject/@tobject.type").InnerText()

                'Dim dato As Date = xmlDoc.SelectSingleNode("nitf/head/docdata/date.issue/@norm").InnerText
                Dim strDatePart As String = xmlDoc.SelectSingleNode("nitf/head/meta[@name='NTBUtDato']/@content").InnerText

                'Dim strDiv As String
                'If InStr(1, strDatePart, "/") > 0 Then
                '    strDiv = "/"
                'Else
                '    strDiv = "."
                'End If

                'Dim intPos1 As Integer = InStr(1, strDatePart, strDiv)
                'Dim intPos2 As Integer = InStr(intPos1 + 1, strDatePart, strDiv)

                ''strDatePart = Mid(strDatePart, intPos2 + 1) & strDiv & Mid(strDatePart, 1, intPos1 - 1) & strDiv & Mid(strDatePart, intPos1 + 1, intPos2 - intPos1 - 1)
                'Dim dtDate As DateTime = New DateTime(Mid(strDatePart, intPos2 + 1), Mid(strDatePart, 1, intPos1 - 1), Mid(strDatePart, intPos1 + 1, intPos2 - intPos1 - 1))

                Dim dtDate As Date
                Try
                    '    dtDate = Convert.ToDateTime(strDatePart)
                    dtDate = Convert.ToDateTime(strDatePart, New System.Globalization.CultureInfo("nb-NO", True))
                Catch
                    '    ' Use Today if date format for this field from Notabene come in a different format in future
                    dtDate = Today
                End Try


                'Dim strDiv As String
                'If InStr(1, strDatePart, "/") > 0 Then
                '    strDiv = "/"
                'Else
                '    strDiv = "."
                'End If

                'Dim intPos1 As Integer = InStr(1, strDatePart, strDiv)
                'Dim intPos2 As Integer = InStr(intPos1 + 1, strDatePart, strDiv)

                ''Dim strMenuFile As String = strType & "_" & Mid(strDatePart, intPos2 + 1) & "-" & Mid(strDatePart, 1, intPos1 - 1) & "-" & Mid(strDatePart, intPos1 + 1, intPos2 - intPos1 - 1) & ".xml"
                'strDatePart = Mid(strDatePart, intPos2 + 1) & strDiv & Mid(strDatePart, 1, intPos1 - 1) & strDiv & Mid(strDatePart, intPos1 + 1, intPos2 - intPos1 - 1)
                'Dim dtDate As DateTime
                'Try
                '    dtDate = Convert.ToDateTime(strDatePart)
                'Catch
                '    ' Use Today if date format for this field from Notabene come in a different format in future
                '    dtDate = Today
                'End Try

                strDatePart = Format(dtDate, "yyyy-MM-dd")

                Dim strMenuFile As String = strType & "_" & strDatePart & ".xml"
                Return strMenuFile
            End If
        Catch err As Exception
            LogFile.WriteErr(SimpleService.errFilePath, "Error in (nitf/head/meta[@name='ntb-folder']/@content) element format", err)
        End Try

        ' Get some important values from the NITF xml-document
        'Dim strMaingroup As String
        Dim intMaingroup As Integer = GetBitPattern(xmlDoc, "Maingroup", False) ', strMaingroup)
        'Dim strSubgroup As String
        Dim intSubgroup As Integer = GetBitPattern(xmlDoc, "Subgroup", False) ', strSubgroup)

        ' Get DateTime value from NITF field: "nitf/head/docdata/date.issue/@norm"
        CreationDateTime = xmlDoc.SelectSingleNode("nitf/head/docdata/date.issue/@norm").InnerText

        ' Test bit pattern for NtbFolder, MainGroup and SubGroup
        Dim bNtbFolder As Boolean = intNtbFolderID And intSqlImportNtbFolder
        Dim bMaingroup As Boolean = intMaingroup And intSqlImportMaingroup
        Dim bSubgroup As Boolean = intSubgroup And intSqlImportSubgroup
        Dim intUrgency As Integer
        Dim ArticleTitle As String
        Dim strTemp As String

        If bMaingroup And bSubgroup And bNtbFolder Then
            ' SQL server Article generation and import

            ' Get the other values from the NITF xml-document

            Dim soundNodeList As XmlNodeList = xmlDoc.SelectNodes("nitf/body/body.content/media[@media-type='audio']")
            Dim intSound As Integer = soundNodeList.Count
            Dim strSoundFile As String = ""
            If intSound > 0 Then
                Try
                    strSoundFile = soundNodeList.Item(0).SelectSingleNode("media-reference[@mime-type='application/x-shockwave-flash']/@source").InnerText
                    strSoundFile = Path.GetFileNameWithoutExtension(strSoundFile)
                Catch err As Exception
                    LogFile.WriteErr(SimpleService.errFilePath, "Error in NITF sound element format", err)
                End Try
            End If

            Dim intPicture As Integer = xmlDoc.SelectNodes("nitf/body/body.content/media[@media-type='image' and media-reference/@source!='']").Count
            intUrgency = xmlDoc.SelectSingleNode("nitf/head/docdata/urgency/@ed-urg").InnerText

            Try
                ArticleTitle = xmlDoc.SelectSingleNode("nitf/body/body.head/hedline").InnerText
            Catch ex As Exception
                ArticleTitle = ""
            End Try

            If ArticleTitle = "" Then
                ' If article title is empty
                Try
                    strTemp = xmlDoc.SelectSingleNode("nitf/body/body.content").InnerText
                Catch
                    strTemp = "Empty XML Body!"
                End Try
                Dim intTemp = Len(strTemp)
                If intTemp > 70 Then
                    intTemp = 50
                End If
                ArticleTitle = "[" & strTemp.Substring(0, intTemp) & "]"
            End If

            Dim Ingress As String
            Try
                Ingress = Trim(xmlDoc.SelectSingleNode("nitf/body/body.content/p[@lede = 'true']").InnerText)
            Catch
                'Ingress = "Empty!"
                Ingress = ""
            End Try

            Dim NTB_ID As String = xmlDoc.SelectSingleNode("nitf/head/docdata/doc-id/@id-string").InnerText

            Dim Keyword As String = xmlDoc.SelectSingleNode("nitf/head/docdata/du-key/@key").InnerText
            ' Keyword itself is also stripped of "FAKTA-" or "HAST-" in sqlBitnames.GetKeywordID(Keyword)
            Dim KeywordID As Integer = 0 'sqlBitnames.GetKeywordID(Keyword)


            'Dim strEvloc As String ' Used for Autonomy, text-value of Evloc
            Dim intEvloc, intCountyDist As Integer
            Try
                intEvloc = GetBitPattern(xmlDoc, "Evloc", False)  ', strEvloc)
                'Dim strCountyDist As String ' Used for Autonomy, text-value of Evloc
                intCountyDist = GetBitPattern(xmlDoc, "CountyDist", True)  ', strCountyDist)
            Catch
            End Try

            '*** Get Categories, Subcategories and Subrels
            Dim intSubrelNum As Integer
            GetCategories(xmlDoc, intCategories, intSubcategories, intHasReferences, arrSubrels, intSubrelNum) ', strAutonomyEtc)

            With Me.SqlInsertCommandNewArticle
                ' Check Which NTB FolderID to hide
                If ((intSqlNotHiddenNtbFolder And intNtbFolderID) <> 0) _
                And ((intSqlNotHiddenMaingroup And intMaingroup) <> 0) Then
                    ' Not Hidden articles
                    .Parameters("@isHidden").Value = 0
                Else
                    ' Hidden articles
                    .Parameters("@isHidden").Value = 1
                End If

                .Parameters("@CreationDateTime").Value = CreationDateTime
                .Parameters("@Urgency").Value = intUrgency

                .Parameters("@HasSounds").Value = intSound
                .Parameters("@SoundFileName").Value = strSoundFile
                .Parameters("@HasPictures").Value = intPicture

                .Parameters("@NtbFolderID").Value = intNtbFolderID

                .Parameters("@NTB_ID").Value = NTB_ID

                .Parameters("@Maingroup").Value = intMaingroup
                .Parameters("@Subgroup").Value = intSubgroup

                .Parameters("@Categories").Value = intCategories
                .Parameters("@Subcategories").Value = intSubcategories
                .Parameters("@HasReferences").Value = intHasReferences

                .Parameters("@Evloc").Value = intEvloc
                .Parameters("@CountyDist").Value = intCountyDist

                .Parameters("@ArticleTitle").Value = ArticleTitle
                .Parameters("@Ingress").Value = Ingress
                .Parameters("@ArticleXML").Value = strXmlDoc 'xmlDoc.OuterXml
                .Parameters("@KeyWordID").Value = KeywordID
                .Parameters("@KeyWord").Value = Keyword

                '*** For debug purpose:
                .Parameters("@Debug").Value = strDebug

                Try
                    intArticleId = .ExecuteScalar()
                Catch e2 As Exception
                    e = e2
                    xmlDoc = Nothing
                    Return "error"
                End Try

            End With

            ' Make Subrelations in SUBREL table
            If intHasReferences = -1 Then ' All bits set = Has Subrels
                With Me.SqlInsertCommandAddSubrel
                    For i = 1 To intSubrelNum
                        'Dim intCat As Integer = arrSubrels(0, i)
                        'Dim intSubcat As Int64 = arrSubrels(1, i)
                        .Parameters("@refid").Value = intArticleId
                        .Parameters("@cat").Value = arrSubrels(0, i)
                        .Parameters("@subcat").Value = arrSubrels(1, i)
                        Try
                            .ExecuteNonQuery()
                        Catch e2 As Exception
                            e = e2
                            xmlDoc = Nothing
                            Return "error"
                        End Try
                    Next

                End With
            End If

            Me.SqlInsertCommandAddSubrel.Dispose()
            Me.SqlInsertCommandNewArticle.Dispose()
            LogFile.WriteLog(SimpleService.logFilePath, strFilename & ": Imported to SQL-server as ID: " & intArticleId)

            ' Autonomy IDX-file generation for Automated links. Only for files Imported to SQL-server!!
            bNtbFolder = intNtbFolderID And intAutonomyImportNtbFolder
            bMaingroup = intMaingroup And intAutonomyImportMaingroup
            bSubgroup = intSubgroup And intAutonomyImportSubgroup
            If bAutonomyImportEnable And (intUrgency > 3) And bMaingroup And bSubgroup And bNtbFolder Then
                ' Only do Autonomy IDX file creation acording to bAutonomyImportEnable AND Bitwise selection of groups etc.
                ' No Autonomy IDX file creation for Urgent messages 
                '   (since they have only very short message, and will often give odd content matching).

                Dim strIdxFile As String = AutonomyIdxImportPath & "/" & Path.GetFileNameWithoutExtension(strFilename) & ".idx"
                fileIdx.Open(strIdxFile)
                fileIdx.MakeIdx32("NTB", intArticleId, strXmlDoc)
                fileIdx.Close()
                LogFile.WriteLog(SimpleService.logFilePath, strIdxFile & ": Written")
            End If

            ' Autonomy IDX-file generation for Archive search. Only for files Imported to SQL-server!!
            bNtbFolder = intNtbFolderID And intAutonomyArchiveNtbFolder
            bMaingroup = intMaingroup And intAutonomyArchiveMaingroup
            bSubgroup = intSubgroup And intAutonomyArchiveSubgroup
            If bAutonomyArchiveEnable And (intUrgency > 3) And bMaingroup And bSubgroup And bNtbFolder Then
                ' Only do Autonomy IDX file creation acording to bAutonomyImportEnable AND Bitwise selection of groups etc.
                ' No Autonomy IDX file creation for Urgent messages 
                '   (since they have only very short message, and will often give odd content matching).

                Dim strIdxFile As String = AutonomyIdxArchivePath & "/" & Path.GetFileNameWithoutExtension(strFilename) & ".idx"
                fileIdx.Open(strIdxFile)
                fileIdx.MakeIdx32("NTB", intArticleId, strXmlDoc)
                fileIdx.Close()
                LogFile.WriteLog(SimpleService.logFilePath, strIdxFile & ": Written")
            End If

        End If

        If intUrgency < 4 And intNtbFolderID = 1 And intSubgroup <> 2 Then
            CreateTicker(intArticleId, intUrgency, ArticleTitle)
        End If

        xmlDoc = Nothing
        Return "ok"
    End Function

    Sub CreateTicker(ByVal intArticleId As Integer, ByVal intUrgency As Integer, ByVal ArticleTitle As String)
        Dim strCmdText As String = "createTicker " & intArticleId & ", " & intUrgency & ", '" & ArticleTitle & "'"
        Dim tickerCommand As SqlClient.SqlCommand = New SqlClient.SqlCommand(strCmdText, Me.SqlConnection1)
        Try
            tickerCommand.ExecuteScalar()
        Catch e As Exception
            LogFile.WriteErr(SimpleService.errFilePath, "ERROR Creating Ticker: " & intArticleId & ": " & ArticleTitle, e)
        End Try
        tickerCommand.Dispose()
    End Sub

    Private Function GetNtbFolder(ByRef xmlDoc As XmlDocument) As Integer

        Dim ret As Integer = 0
        Dim strNtbFolder As String

        'Temporary: check foldername
        Try
            strNtbFolder = xmlDoc.SelectSingleNode("nitf/head/meta[@name='foldername']/@content").InnerText
            ret = sqlBitnames.GetNtbFolderID(strNtbFolder)
        Catch
        End Try

        'Check tjeneste
        If ret = 0 Then
            Try
                strNtbFolder = xmlDoc.SelectSingleNode("nitf/head/meta[@name='NTBTjeneste']/@content").InnerText
                ret = sqlBitnames.GetNtbFolderID(strNtbFolder)
            Catch
            End Try
        End If

        Return ret
    End Function

    Private Function GetBitPattern(ByRef xmlDoc As XmlDocument, ByVal strTypeText As String, ByVal bSubcat As Boolean) As Integer ', ByRef strNITFCont As String) As Integer
        Dim strNitfXPath As String
        Dim strNITFCont As String
        Dim intBitmap As Integer = 0

        ' Lookup NITF XPath "Query-string"
        strNitfXPath = sqlBitnames.GetNitfXPath(strTypeText)

        ' Get the value from XML Element or Attribute
        'strNITFCont = xmlDoc.SelectNodes(strNitfXPath).InnerText

        '*** For debuging:
        strDebug &= strTypeText & "=" & strNITFCont & ", "

        Dim node As XmlNode
        For Each node In xmlDoc.SelectNodes(strNitfXPath)
            strNITFCont = node.InnerText

            If bSubcat Then
                intBitmap = intBitmap Or sqlBitnames.GetBitmapSub(strNITFCont)
            Else
                intBitmap = intBitmap Or sqlBitnames.GetBitmap(strNITFCont)
            End If
        Next


        '' Get The bit pattern for this XML element
        'If strNITFCont.EndsWith(";") Then
        '    ' For multiple values in one string separated with ";"
        '    Dim arrValues() As String = Split(strNITFCont, ";")
        '    Dim i As Integer
        '    For i = 0 To arrValues.Length - 2
        '        strNITFCont = arrValues(i)
        '        If bSubcat Then
        '            intBitmap = intBitmap Or sqlBitnames.GetBitmapSub(strNITFCont)
        '        Else
        '            intBitmap = intBitmap Or sqlBitnames.GetBitmap(strNITFCont)
        '        End If
        '    Next
        '    Return intBitmap
        'Else
        '    ' For single values of Category and SubCategory
        '    If bSubcat Then
        '        ' Values of type Subcat
        '        intBitmap = sqlBitnames.GetBitmapSub(strNITFCont)
        '    Else
        '        ' Values of type Category
        '        intBitmap = sqlBitnames.GetBitmap(strNITFCont)
        '    End If
        'End If

        'If intBitmap = 0 Then
        ' Set all bits (= -1) if no value match
        '    intBitmap = -1
        'End If

        Return intBitmap
    End Function

    Private Function GetCategories(ByRef xmlDoc As XmlDocument, ByRef intCategories As Integer, ByRef intSubcategories As Int64, ByRef intHasReferences As Integer, ByRef intSubrels(,) As Int64, ByRef intSubrelNum As Integer) As Boolean ', ByRef strAutonomyEtc As String) As Boolean
        ' Get The bit pattern for Categories, subcategorise and make Subrel array
        Dim strNitfXPath As String
        Dim strNITFCont As String
        Dim strSubcat As String
        Dim strSubcatList As String
        'Dim intSubrelNum As Integer = 0
        Dim intCatnum As Integer = 0
        Dim intCat As Integer = 0
        Dim intOneCat As Integer = 0
        Dim i As Integer

        intSubrelNum = 0
        intHasReferences = 0

        intCategories = 0
        intSubcategories = 0

        strNitfXPath = sqlBitnames.GetNitfXPath("Categories")

        Dim node As XmlNode
        For Each node In xmlDoc.SelectNodes(strNitfXPath)
            strNITFCont = node.Attributes.GetNamedItem("tobject.subject.code").InnerText

            intCat = sqlBitnames.GetBitmap(strNITFCont)

            Dim bIsDublicate As Boolean = intCat And intCategories
            If Not bIsDublicate Then
                intCategories = intCategories Or intCat
                strDebug &= "Categories" & "=" & strNITFCont & ", "

                intCatnum += 1
            End If

            Dim intSubcategories2 As Int64 = 0

            Try
                strSubcatList = node.Attributes.GetNamedItem("tobject.subject.matter").InnerText
                If strSubcatList <> "" Then
                    ' strAutonomyEtc &= strSubcatList 'For Autonumy
                    strDebug &= "Subcat" & "=" & strSubcatList & ", "
                    intSubrelNum += 1
                    intOneCat = intCat

                    Dim tmpSubcategory As Int64 = sqlBitnames.GetBitmapSub(strSubcatList)
                    intSubcategories = intSubcategories Or tmpSubcategory
                    intSubcategories2 = intSubcategories2 Or tmpSubcategory

                    intSubrels(0, intSubrelNum) = intCat
                    intSubrels(1, intSubrelNum) = intSubcategories2
                End If
            Catch
                ' Do nothing
            End Try
        Next

        ' Check if More than on Catagory has Subcatagories
        If intCatnum = 1 And intSubrelNum = 0 Then
            ' Only one Category
            intHasReferences = 0
        ElseIf intCatnum > 1 And intSubrelNum > 1 Then
            ' More than one Categoy with Subcategories
            intHasReferences = -1
            intSubcategories = 0
            'intSubrels(0, 0) = intSubrelNum
        ElseIf intCatnum > 1 And intSubrelNum = 1 Then
            ' Only one Categoy with Subcategories
            intHasReferences = intOneCat
        Else
            intHasReferences = 0
        End If

        Return True
    End Function

End Class
