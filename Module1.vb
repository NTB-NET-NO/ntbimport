Imports System
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib

#If DEBUG Then
Public Module SimpleService
#Else
Public Class SimpleService : Inherits ServiceBase
#End If

    '*** Global variables:
    Public doneNitfPath As String
    Public errorNitfPath As String
    Public skipNitfPath As String

#If Debug Then
    Public errFilePath As String
    Public logFilePath As String
#Else
    Public Shared errFilePath As String
    Public Shared logFilePath As String
#End If

    Public menuFilePath As String

    Public version As Integer

    '*** Module variables:
    Private sqlArticle32 As AddArticle32
    Private sqlArticle As AddArticle

    Private importNitfPath As String
    Private interval As Integer

    'Public FSW1 As FileSystemWatcher
    Friend WithEvents FSW1 As System.IO.FileSystemWatcher
    Friend WithEvents PollTimer As System.Timers.Timer

    'Win API function Sleep
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

#If Debug Then
    Public Sub Main()
        InitConfigValues()
        EventLog.WriteEntry("NTB_Portal_Import", "NTB_Import Service started")
        ImportAll()
        EventLog.WriteEntry("NTB_Portal_Import", "Kj�rt ImportAll")
    End Sub
#Else
    Public Shared Sub Main()
        ServiceBase.Run(New SimpleService())
    End Sub

    Public Sub New()
        MyBase.New()
        CanPauseAndContinue = True
        ServiceName = "NTB_Import"
    End Sub

    Protected Overrides Sub OnStop()
        FSW1.EnableRaisingEvents = False
        Sleep(5000)
        On Error Resume Next
        LogFile.WriteLog(logFilePath, "NTB_Import Service stopped")
        EventLog.WriteEntry("NTB_Import Service stopped")
        sqlArticle.Dispose()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)

        '*** Init global variables
        Try
            FSW1 = New FileSystemWatcher()
            Polltimer = new System.Timers.Timer()

            InitConfigValues()

            '*** Enable filesystem watcher for Filecreation interrupt
            FSW1.IncludeSubdirectories = False
            FSW1.Filter = "*.xml"
            FSW1.Path = importNitfPath

            '*** Enable timer
            polltimer.interval = interval

            Try
                LogFile.WriteLog(logFilePath, "NTB_Import Service Started")
                EventLog.WriteEntry("NTB_Import Service started")
            Catch
            End Try

            StartJob()

        Catch err As Exception
            Dim strErr As String = "Error in Initialization, program aborted"
            Try
                LogFile.WriteErr(errFilePath, strErr, err)
                EventLog.WriteEntry(strErr)
            Catch
            End Try
            ' End Program
            End
        End Try
    End Sub
#End If

    Private Sub InitConfigValues()
        ' Reading Config values from "NTB_Import.exe.config" file
        importNitfPath = AppSettings("importNitfPath")
        doneNitfPath = AppSettings("doneNitfPath")
        skipNitfPath = AppSettings("skipNitfPath")
        errorNitfPath = AppSettings("errorNitfPath")
        logFilePath = AppSettings("logFilePath")
        errFilePath = AppSettings("errFilePath")
        version = AppSettings("NITFVersion")
        interval = AppSettings("PollingInterval") * 1000

        If Not Directory.Exists(importNitfPath) Then
            Directory.CreateDirectory(importNitfPath)
        End If

        If Not Directory.Exists(doneNitfPath) Then
            Directory.CreateDirectory(doneNitfPath)
        End If

        If Not Directory.Exists(skipNitfPath) Then
            Directory.CreateDirectory(skipNitfPath)
        End If

        If Not Directory.Exists(errorNitfPath) Then
            Directory.CreateDirectory(errorNitfPath)
        End If

        If Not Directory.Exists(logFilePath) Then
            Directory.CreateDirectory(logFilePath)
        End If

        If Not Directory.Exists(errFilePath) Then
            Directory.CreateDirectory(errFilePath)
        End If

        menuFilePath = AppSettings("menuFilePath")
        'If Not Directory.Exists(menuFilePath) Then
        '    Directory.CreateDirectory(menuFilePath)
        'End If

        sqlArticle = New AddArticle()
        sqlArticle32 = New AddArticle32()

    End Sub

    Private Sub StartJob()
        ' Start service
        LogFile.WriteLog(SimpleService.errFilePath, "Vi er i StartJob")
        ' All files waiting in import folder will be imported:
        sqlArticle.RefreshKeywords()
        ImportAll()
        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med ImportAll i StartJob (module1.vb)")
        ' "Sub FSW1_Created" will now trigger on Filesystem event: Created
        ' and do all new files appearing from now on:
        FSW1.EnableRaisingEvents = True
        PollTimer.Enabled = True
        LogFile.WriteLog(SimpleService.errFilePath, "Vi er ferdig med StartJob")
    End Sub

    Sub FSW1_Created(ByVal sender As Object, ByVal ev As System.IO.FileSystemEventArgs) Handles FSW1.Created
        ' Disable events to prevent multi-threading conflicts!
        FSW1.EnableRaisingEvents = False
        PollTimer.Enabled = False

        Console.WriteLine(Now & ": New File Created: " & ev.Name)

        Try
            'sqlArticle.RefreshKeywords()
            ImportAll()
            LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med ImportAll i FSW1_Created (module1.vb)")
        Catch e As Exception
            LogFile.WriteErr(errFilePath, "Error open SQL-server", e)
        End Try


        ' Enable events when all files have been imported
        ' Extra importjob in case of more files arrived since last event

        PollTimer.Enabled = True
        FSW1.EnableRaisingEvents = True
    End Sub

    Public Sub Timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed

        ' Disable events to prevent multi-threading conflicts!
        FSW1.EnableRaisingEvents = False
        PollTimer.Enabled = False

        Try
            'sqlArticle.RefreshKeywords()
            ImportAll()
            LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med Timer_Elapsed ImportAll (module1.vb)")
        Catch ex As Exception
            LogFile.WriteErr(errFilePath, "Error open SQL-server", ex)
        End Try

        ' Enable events when all files have been imported
        ' Extra importjob in case of more files arrived since last event

        PollTimer.Enabled = True
        FSW1.EnableRaisingEvents = True
    End Sub

    Sub ImportAll()
        ' Process the list of files found in the directory
        LogFile.WriteLog(SimpleService.errFilePath, "Starter ImportAll (module1.vb)")
        LogFile.WriteLog(SimpleService.errFilePath, "importNitfPath (module1.vb): " & importNitfPath)
        Dim fileEntries As String() = Directory.GetFiles(importNitfPath, "*.xml")

        If fileEntries.Length = 0 Then
            Return
        End If
        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med fileEntries" & fileEntries.Length & " (module1.vb)")
        '*** Wait 5 seconds for all files to be
        Sleep(5000)

        If version = 32 Then
            sqlArticle32.Open()
        Else
            sqlArticle.Open()
        End If

        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med SQLArticle.open (module1.vb)")
        Try
            Try
                Dim strFileName As String
                For Each strFileName In fileEntries
                    ImportOneFile(strFileName)
                Next
            Catch e As Exception
                LogFile.WriteErr(errFilePath, "Error in ImportOneFile", e)
            End Try
        Catch e As Exception
            LogFile.WriteErr(errFilePath, "Error open SQL-server", e)
        End Try
        refreshCache()

        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med ImportOneFile og refreshCache (module1.vb)")
        If version = 32 Then
            sqlArticle32.Close()
        Else
            sqlArticle.Close()
        End If
        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med SQLArticle.close (module1.vb)")
        LogFile.WriteLog(SimpleService.errFilePath, "ImportAll (module1.vb)")
    End Sub

    Sub ImportOneFile(ByRef strFileName As String)
        ' Process the list of files found in the directory
        LogFile.WriteLog(SimpleService.errFilePath, "Starter ImportOneFile (module1.vb)")
        'Dim IsImported As Boolean
        Dim strStatus As String
        Dim errImport As Exception

        Dim dtTimeStamp As DateTime = "2000.01.01"

        If version = 32 Then
            strStatus = sqlArticle32.AddNewArticle(strFileName, errImport, dtTimeStamp)
        Else
            strStatus = sqlArticle.AddNewArticle(strFileName, errImport, dtTimeStamp)
        End If

        Select Case strStatus
            Case "ok"
                ' File is imported OK
                Try
                    Dim strDoneFile As String = FuncLib.MakeSubDirDate(doneNitfPath & "\" & Path.GetFileName(strFileName), dtTimeStamp)
                    File.Copy(strFileName, strDoneFile, True)

                    '#If Not Debug Then
                    File.Delete(strFileName)
                    '#End If

                Catch e As Exception
                    ' Error on moving imported file
                    LogFile.WriteErr(errFilePath, "ERROR moving file:", e)
                End Try
            Case "skip"
                ' File is not imported
                Try
                    File.Copy(strFileName, skipNitfPath & "\" & Path.GetFileName(strFileName), True)
                    File.Delete(strFileName)
                Catch e As Exception
                    ' Error on moving imported file
                    LogFile.WriteErr(errFilePath, "ERROR moving file:", e)
                End Try
            Case "error"
                ' File NOT imported because of ERROR, error  
                LogFile.WriteErr(errFilePath, "ERROR importing:" & strFileName, errImport)
                Try
                    File.Move(strFileName, errorNitfPath & "\" & Path.GetFileName(strFileName))
                Catch e As Exception
                    LogFile.WriteErr(errFilePath, "ERROR moving file:" & strFileName, e)
                End Try

            Case Else
                ' File not imported but moved with new name to Portal
                Try
                    Dim strDoneFile As String = menuFilePath & "\" & strStatus
                    File.Copy(strFileName, strDoneFile, True)
                    File.Delete(strFileName)
                Catch e As Exception
                    ' Error on moving imported file
                    LogFile.WriteErr(errFilePath, "ERROR moving file:", e)
                End Try
        End Select

        LogFile.WriteLog(SimpleService.errFilePath, "Ferdig med ImportOneFile (module1.vb)")
    End Sub

    Sub refreshCache()
        Dim strCmdText As String = "refreshCache"
        Dim refreshCacheCommand As SqlClient.SqlCommand

        If version = 32 Then
            refreshCacheCommand = New SqlClient.SqlCommand(strCmdText, sqlArticle32.SqlConnection1)
        Else
            refreshCacheCommand = New SqlClient.SqlCommand(strCmdText, sqlArticle.SqlConnection1)
        End If

        Try
            refreshCacheCommand.ExecuteScalar()
        Catch e As Exception
            LogFile.WriteErr(errFilePath, "ERROR updating cache!", e)
        End Try

        refreshCacheCommand.Dispose()
    End Sub

#If Debug Then
End Module
#Else
End Class
#End If
